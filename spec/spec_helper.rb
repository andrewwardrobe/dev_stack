require "bundler/setup"
require "dev_stack"
require 'serverspec_launcher/spec_helper'
require 'serverspec_extra_types'

def domain
  property[:variables][:domain] || 'local'
end


def ldap_dn
  domain.split('.').map { |part| "dc=#{part}" }.join(',')
end

def stack
  property[:variables][:stack_name] || 'dev'
end

RSpec.configure do |config|
  # Enable flags like --only-failures and --next-failure
  config.example_status_persistence_file_path = ".rspec_status"

  # Disable RSpec exposing methods globally on `Module` and `main`
  #config.disable_monkey_patching!

  config.expect_with :rspec do |c|
    c.syntax = :expect
  end
end
