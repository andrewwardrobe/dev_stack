require 'spec_helper'

shared_examples "dev_stack::services" do

  describe docker_service("#{stack}_traefik") do
    it { should exist }
    it { should have_network 'traefik' }
    it { should map_port('80','80').using_protocol('tcp').with_mode('host') }
    it { should map_port('443','443').using_protocol('tcp').with_mode('host') }
    it { should map_port('8080','8080').using_protocol('tcp').with_mode('host') }
    it { should have_image('traefik:1.7') }
    it { should have_mount('/var/run/docker.sock', '/var/run/docker.sock')}
    it { should have_restart_policy('on-failure') }
    it { should have_restart_limit(0) }
    it { should be_replicated }
    it { should have_replica_count 1 }
    it { should have_secret("#{stack}_#{domain}.key")}
    it { should have_secret("#{stack}_#{domain}.crt")}
    it { should have_label('traefik.port').with_value('8080')}
    it { should have_label('traefik.frontend.rule').with_value("Host:traefik.#{domain}")}
    it { should have_label('traefik.frontend.entryPoints').with_value('http')}
    it { should have_label('traefik.frontend.redirect.entryPoint').with_value('http')}
    it { should have_placement_constraint('node.role == manager') }
  end

  describe docker_service("#{stack}_consul") do
    it { should exist }
    it { should have_image('consul:latest') }
    it { should have_mount("#{stack}_consul-data", '/consul/data')}
    it { should have_restart_policy('on-failure') }
    it { should have_restart_limit(0) }
    it { should be_replicated }
    it { should have_replica_count 1 }
    it { should have_label('traefik.port').with_value('8500')}
    it { should have_label('traefik.frontend.rule').with_value("Host:consul.#{domain}")}
    it { should have_label('traefik.frontend.entryPoints').with_value('http,https')}
    it { should have_label('traefik.frontend.redirect.entryPoint').with_value('https')}
    it { should have_placement_constraint('node.role == manager') }
  end


  describe docker_service("#{stack}_gitlab") do
    it { should exist }
    it { should have_image('gitlab/gitlab-ce:latest') }
    it { should have_mount("#{stack}_gitlab-data", '/var/opt/gitlab')}
    it { should have_mount("#{stack}_gitlab-logs", '/var/log/gitlab')}
    it { should have_mount("#{stack}_gitlab-config", '/etc/gitlab')}
    it { should have_network('traefik')}
    it { should have_network("#{stack}_gitlab")}
    it { should have_network("#{stack}_ldap")}
    it { should have_restart_policy('on-failure') }
    it { should have_restart_limit(0) }
    it { should be_replicated }
    it { should have_replica_count 1 }
    it { should have_config("#{stack}_gitlab", '/omnibus_config.rb')}
    it { should have_label('traefik.port').with_value('80')}
    it { should have_label('traefik.docker.network').with_value('traefik')}
    it { should have_label('traefik.protocol').with_value('http')}
    it { should have_label('traefik.frontend.rule').with_value("Host:gitlab.#{domain}")}
    it { should have_label('traefik.frontend.entryPoints').with_value('http,https')}
    it { should have_label('traefik.frontend.redirect.entryPoint').with_value('https')}
    it { should have_environment_variable('GITLAB_HTTPS').with_value('false')}
    it { should have_environment_variable('GITLAB_OMNIBUS_CONFIG').with_value("from_file('/omnibus_config.rb')")}
    it { should have_placement_constraint('node.role == worker') }
  end

  describe docker_service("#{stack}_openldap") do
    it { should exist }
    it { should have_image 'osixia/openldap:1.2.2' }
    it { should have_mount("#{stack}_ldap-slap.d", '/etc/ldap/slapd.d') }
    it { should have_mount("#{stack}_ldap-data", '/var/lib/ldap') }
    it { should have_network("#{stack}_ldap")}
    it { should have_restart_policy('on-failure') }
    it { should have_restart_limit(0) }
    it { should be_replicated }
    it { should have_replica_count 1 }
    it { should have_placement_constraint('node.role == worker') }
    it { should have_environment_variable('LDAP_DOMAIN').with_value(domain)}
  end

  describe docker_service("#{stack}_phpldapadmin") do
    it { should exist }
    it { should have_image 'osixia/phpldapadmin:latest' }
    it { should have_network('traefik')}
    it { should have_network("#{stack}_ldap")}
    it { should have_config("#{stack}_#{domain}.crt", "/container/service/phpldapadmin/assets/apache2/certs/#{ domain }.crt")}
    it { should have_config("#{stack}_#{domain}.key", "/container/service/phpldapadmin/assets/apache2/certs/#{ domain }.key")}
    it { should have_restart_policy('on-failure') }
    it { should have_restart_limit(0) }
    it { should be_replicated }
    it { should have_replica_count 1 }
    it { should have_placement_constraint('node.role == worker') }
    it { should have_environment_variable('PHPLDAPADMIN_LDAP_HOSTS').with_value('openldap')}
    it { should have_environment_variable('PHPLDAPADMIN_HTTPS').with_value('true')}
    it { should have_environment_variable('PHPLDAPADMIN_HTTPS_KEY_FILENAME').with_value("#{domain}.key")}
    it { should have_environment_variable('PHPLDAPADMIN_HTTPS_CRT_FILENAME').with_value("#{domain}.crt")}
    it { should have_label('traefik.port').with_value('443')}
    it { should have_label('traefik.docker.network').with_value('traefik')}
    it { should have_label('traefik.protocol').with_value('https')}
    it { should have_label('traefik.frontend.rule').with_value("Host:phpldapadmin.#{domain}")}
    it { should have_label('traefik.frontend.entryPoints').with_value('http,https')}
    it { should have_label('traefik.frontend.redirect.entryPoint').with_value('https')}
  end

  describe docker_service("#{stack}_gerrit") do
    it { should exist }
    it { should have_image 'openfrontier/gerrit:2.15.6-slim' }
    it { should have_network('traefik')}
    it { should have_network("#{stack}_ldap")}
    it { should have_mount("#{stack}_gerrit-site", '/var/gerrit/review_site') }
    it { should have_restart_policy('on-failure') }
    it { should have_restart_limit(0) }
    it { should be_replicated }
    it { should have_replica_count 1 }
    it { should have_placement_constraint('node.role == worker') }
    it { should have_environment_variable('WEBURL').with_value("http://gerrit.#{domain}")}
    it { should have_environment_variable('GITWEB_TYPE').with_value('gitiles')}
    it { should have_environment_variable('AUTH_TYPE').with_value('LDAP')}
    it { should have_environment_variable('LDAP_SERVER').with_value('ldap://openldap')}
    it { should have_environment_variable('LDAP_ACCOUNTBASE').with_value("ou=people,#{ldap_dn}")}
    it { should have_environment_variable('LDAP_GROUPBASE').with_value("ou=groups,#{ldap_dn}")}
    it { should have_environment_variable('LDAP_USERNAME').with_value("cn=admin,#{ldap_dn}")}
    it { should have_environment_variable('LDAP_PASSWORD').with_value("admin")}
    it { should have_environment_variable('LDAP_ACCOUNTFULLNAME').with_value('${cn}')}
    it { should have_label('traefik.port').with_value('8080')}
    it { should have_label('traefik.docker.network').with_value('traefik')}
    it { should have_label('traefik.frontend.rule').with_value("Host:gerrit.#{domain}")}
    it { should have_label('traefik.frontend.entryPoints').with_value('http,https')}
    it { should have_label('traefik.frontend.redirect.entryPoint').with_value('https')}
  end

  describe docker_service("#{stack}_jenkins") do
    it { should exist }
    it { should have_image 'jenkins/jenkins:lts' }
    it { should have_network('traefik')}
    it { should have_network("#{stack}_ldap")}
    it { should have_mount("#{stack}_jenkins-data", '/var/jenkins_home') }
    it { should have_restart_policy('on-failure') }
    it { should have_restart_limit(0) }
    it { should be_replicated }
    it { should have_replica_count 1 }
    it { should have_placement_constraint('node.role == worker') }
    it { should have_environment_variable('JAVA_OPTS').with_value("-Djenkins.install.runSetupWizard=false")}
    it { should have_environment_variable('VIRTUAL_HOST').with_value("jenkins.#{domain}")}
    it { should have_config("#{stack}_plugins.groovy", '/usr/share/jenkins/ref/init.groovy.d/plugins.groovy') }
    it { should have_config("#{stack}_security.groovy", '/usr/share/jenkins/ref/init.groovy.d/security.groovy') }
    it { should have_label('traefik.port').with_value('8080')}
    it { should have_label('traefik.docker.network').with_value('traefik')}
    it { should have_label('traefik.frontend.rule').with_value("Host:jenkins.#{domain}")}
    it { should have_label('traefik.frontend.entryPoints').with_value('http,https')}
    it { should have_label('traefik.frontend.redirect.entryPoint').with_value('https')}
  end

  describe docker_service("#{stack}_visualizer") do
    it { should exist }
    it { should have_image 'dockersamples/visualizer:latest' }
    it { should have_network('traefik')}
    it { should have_mount('/var/run/docker.sock', '/var/run/docker.sock')}
    it { should have_restart_policy('on-failure') }
    it { should have_restart_limit(0) }
    it { should be_replicated }
    it { should have_replica_count 1 }
    it { should have_placement_constraint('node.role == manager') }
  
    it { should have_label('traefik.port').with_value('8080')}
    it { should have_label('traefik.frontend.rule').with_value("Host:visualizer.#{domain}")}
    it { should have_label('traefik.frontend.entryPoints').with_value('http,https')}
    it { should have_label('traefik.frontend.redirect.entryPoint').with_value('https')}
  end
  
  describe docker_service("#{stack}_puppet") do
    it { should exist }
    it { should have_image 'puppet/puppetserver:latest' }
    it { should have_mount("#{stack}_puppet-code", '/etc/puppetlabs/code')}
    it { should have_mount("#{stack}_puppet-ssl", '/etc/puppetlabs/puppet/ssl')}
    it { should have_mount("#{stack}_puppet-data", '/opt/puppetlabs/server/data/puppetserver')}
    it { should have_restart_policy('on-failure') }
    it { should have_restart_limit(0) }
    it { should be_replicated }
    it { should have_replica_count 1 }
    it { should have_placement_constraint('node.role == worker') }
    it { should map_port('8140', '8140')}
 end

  describe docker_service("#{stack}_portainer") do
    it { should exist }
    it { should have_image 'portainer/portainer:latest' }
    it { should have_network('traefik')}
    it { should have_mount('/var/run/docker.sock', '/var/run/docker.sock')}
    it { should have_restart_policy('on-failure') }
    it { should have_restart_limit(0) }
    it { should be_replicated }
    it { should have_replica_count 1 }
    it { should have_placement_constraint('node.role == manager') }

    it { should have_label('traefik.port').with_value('9000')}
    it { should have_label('traefik.frontend.rule').with_value("Host:portainer.#{domain}")}
    it { should have_label('traefik.frontend.entryPoints').with_value('http,https')}
    it { should have_label('traefik.frontend.redirect.entryPoint').with_value('https')}
  end
end
