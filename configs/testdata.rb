#!/usr/bin/env ruby

require 'rubygems'
require 'net/ldap'

class LDAP

  def initialize
    @people_base = 'ou=people,{{ domain_name | ldap_dn }}'
    @group_base = 'ou=groups,{{ domain_name | ldap_dn }}'

    @ldap = Net::LDAP.new host: 'openldap',
                         port: 389,
                         base: '{{ domain_name | ldap_dn }}',
                         auth: {
                             :method => :simple,
                             :username => "cn=admin,{{ domain_name | ldap_dn }}",
                             :password => "{{ ldap_admin_password }}"
                         }
    add_ou('people')
    add_ou('groups')
  end
  def add_ou(ou)

    result = @ldap.add(dn: "ou=#{ou},{{ domain_name | ldap_dn }}", attributes: {ou: ou, objectClass: 'organizationalUnit'})
    unless result
      puts("Failed to add ou: #{@ldap.get_operation_result}")
    end
  end

  def add_group(attrs)
    dn = "cn=#{attrs[:cn]},#{@group_base}"
    attrs[:objectClass] = %w{top posixGroup}
    result = @ldap.add(:dn => dn, :attributes => attrs)
    unless result
      puts("Failed to add group: #{@ldap.get_operation_result}")
    end
  end


  def add_user(attrs)
    dn = "cn=#{attrs[:cn]},#{@people_base}"
    attrs[:objectClass] = %w{top posixAccount inetOrgPerson},
    result = @ldap.add(:dn => dn, :attributes => attrs)
    unless result
      puts("Failed to add user: #{@ldap.get_operation_result}")
    end
  end

  def add_user_to_group(uid, group)
    filter = Net::LDAP::Filter.eq("cn", group)
    @ldap.search(:base => @group_base, :filter => filter).each do |entry|
      operations = [
          [:add, :memberUid, uid]
      ]
      @ldap.modify(
          :dn => entry.dn,
          :operations => operations
      )
    end
  end
end

attrs = {
    gn: 'Test',
    sn: 'User',
    cn: 'Test User',
    mail: 'test.user@local.org',
    homeDirectory: '/home/test.user',
    loginShell: '/bin/bash',
    uid: 'test.user',
    uidNumber: '1000',
    userPassword: Net::LDAP::Password.generate(:md5, 'password'),
    gidNumber: '500',
    objectClass:  %w{top posixAccount inetOrgPerson}
}

puts 'Hello World'
begin
  ldap = LDAP.new
  ldap.add_group(cn: 'developers', gidNumber: '500')
  ldap.add_user(attrs)
rescue StandardError => ex
  puts ex
end
