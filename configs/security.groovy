#!groovy
{% if jenkins_ldap %}
import hudson.model.*;
import hudson.security.*;
import jenkins.model.*;
import jenkins.security.plugins.ldap.*;
import hudson.util.Secret;
import java.util.logging.Logger


def instance = Jenkins.getInstance()
def logger = Logger.getLogger("")

logger.info('Running Ldap Security config')

if(!(instance.securityRealm instanceof LDAPSecurityRealm)) {
    logger.info('Setting security realm to ldap')

    def ldapConf = new LDAPConfiguration(
            'openldap',
            '{{ domain_name | ldap_dn }}',
            false,
            'cn=admin,{{ domain_name | ldap_dn }}',
            Secret.fromString('{{ ldap_admin_password }}')
    )
    ldapConf.userSearchBase = '{{ ldap_user_search_base }}'
    ldapConf.userSearch = '{{ ldap_user_filter }}'
    ldapConf.groupSearchBase = '{{ ldap_group_search_base }}'
    ldapConf.groupSearchFilter = '{{ ldap_group_filter }}'
    ldapConf.displayNameAttributeName = '{{ ldap_display_name_attribute }}'
    ldapConf.mailAddressAttributeName= '{{ ldap_mail_attribute }}'
    List<LDAPConfiguration> configurations = [ldapConf]

    instance.setSecurityRealm(new LDAPSecurityRealm(
            configurations,
            false,
            null,
            IdStrategy.CASE_INSENSITIVE,
            IdStrategy.CASE_INSENSITIVE
    ))

    def authStrategy = Hudson.instance.getAuthorizationStrategy()

    if (authStrategy instanceof AuthorizationStrategy.Unsecured) {
        instance.setAuthorizationStrategy(new FullControlOnceLoggedInAuthorizationStrategy())
    }

    instance.save()
    logger.info 'Security realm set to LDAP.'
}
else {
    logger.info 'Nothing changed.  LDAP security realm already configured.'
}
{% else %}

import jenkins.model.*
import hudson.security.*
import jenkins.security.s2m.AdminWhitelistRule
import java.util.logging.Logger

def instance = Jenkins.getInstance()
def logger = Logger.getLogger("")

//def user = new File("/run/secrets/jenkins-user").text.trim()
//def pass = new File("/run/secrets/jenkins-pass").text.trim()
def user = "admin"
def pass = "admin"
def hudsonRealm = new HudsonPrivateSecurityRealm(false)
hudsonRealm.createAccount(user, pass)
instance.setSecurityRealm(hudsonRealm)

def strategy = new FullControlOnceLoggedInAuthorizationStrategy()
instance.setAuthorizationStrategy(strategy)
instance.save()

Jenkins.instance.getInjector().getInstance(AdminWhitelistRule.class).setMasterKillSwitch(false)

logger.info 'Seciurty set to jenkins managed'
// https://raw.githubusercontent.com/samrocketman/jenkins-bootstrap-shared/master/scripts/configure-ldap-settings.groovy
{% endif %}