def nfs_export(directories, permissions, prefix = ''):
    return map(lambda x: prefix + x +" "+permissions, directories)



class FilterModule(object):

    def filters(self):
        return {
            'nfs_export': nfs_export
        }