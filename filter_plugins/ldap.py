def ldap_dn(collection):
    return ",".join(map(lambda x: "dc="+x, collection.split(".")))



class FilterModule(object):

    def filters(self):
        return {
            'ldap_dn': ldap_dn
        }