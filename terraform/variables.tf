variable "region" {
  description = "AWS Region"
  default     = "eu-west-2"
}

variable "state_bucket" {
  description = "Bucket for storing state file"
}

variable "state_file" {
  description = "file for storing VPC state"
}

variable "key" {
  description = "SSH used for Provisioning Servers"
}

variable "subnet_ids" {
  description = "Subnet in which to place environment"
  type        = "list"
}

variable "security_groups" {
  description = "Securuty Groups"
  type        = "list"
}

variable "ami_id" {
  description = "AMI ID"
  default     = "ami-f976839e"
}

variable "profile" {
  default     = "acn"
  description = "AWS Profile to use"
}

variable "dns_entry" {
  description = "Put an entry in route 53"
  default     = true
}

variable "route_53_zone" {
  description = "route_53_zone"
  default     = ""
}
