terraform {
  backend "s3" {}
}

provider "aws" {
  region  = "${var.region}"
  profile = "${var.profile}"
}

data "terraform_remote_state" "state" {
  backend = "s3"

  config {
    bucket  = "${var.state_bucket}"
    region  = "${var.region}"
    key     = "${var.state_file}"
    profile = "${var.profile}"
  }
}

module "managers" {
  source          = "./modules/ec2_with_dns"
  name            = "swarm_manager_"
  num_instances   = "3"
  subnet_ids      = "${var.subnet_ids}"
  security_groups = ["${var.security_groups}"]
  route53_zone    = "${var.route_53_zone}"
  key_name        = "${var.key}"
  ami             = "${var.ami_id}"
  roles           = ["swarm-manager"]
  dns_entry       = "${var.dns_entry}"
}

module "workers" {
  source          = "./modules/ec2_with_dns"
  name            = "swarm_worker_"
  num_instances   = "5"
  subnet_ids      = "${var.subnet_ids}"
  security_groups = ["${var.security_groups}"]
  route53_zone    = "${var.route_53_zone}"
  key_name        = "${var.key}"
  ami             = "${var.ami_id}"
  roles           = ["swarm-worker"]
  dns_entry       = "${var.dns_entry}"
}

module "storage" {
  source          = "./modules/ec2_with_dns"
  name            = "fileserver_"
  num_instances   = "1"
  subnet_ids      = "${var.subnet_ids}"
  security_groups = ["${var.security_groups}"]
  route53_zone    = "${var.route_53_zone}"
  key_name        = "${var.key}"
  ami             = "${var.ami_id}"
  roles           = ["nfs-server"]
  dns_entry       = "${var.dns_entry}"
}
